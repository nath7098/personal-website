## 🐛 Bug Report

### Description
Describe the issue you encountered in detail.

### Steps to Reproduce
1. Step 1
2. Step 2
3. Step 3
   ...

### Expected Behavior
Describe what you expected to happen.

### Actual Behavior
Describe what actually happened.

### Additional Information
Add any other context or screenshots about the bug here.

/label ~"Bug" ~"Backlog"
/cc @nath7098